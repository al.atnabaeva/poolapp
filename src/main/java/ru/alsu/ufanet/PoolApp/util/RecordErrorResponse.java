package ru.alsu.ufanet.PoolApp.util;

public class RecordErrorResponse {
    private String message;

    public RecordErrorResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
