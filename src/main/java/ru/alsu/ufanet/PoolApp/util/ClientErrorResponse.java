package ru.alsu.ufanet.PoolApp.util;

public class ClientErrorResponse {
    private String message;

    public ClientErrorResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
