package ru.alsu.ufanet.PoolApp.util;

public class RecordSaveDTONotCreateException extends RuntimeException {
    public RecordSaveDTONotCreateException(String msg) {
        super(msg);
    }
}
