package ru.alsu.ufanet.PoolApp.util;

public class ClientNotCreateException extends RuntimeException {
    public ClientNotCreateException(String msg) {
        super(msg);
    }
}
