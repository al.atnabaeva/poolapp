package ru.alsu.ufanet.PoolApp.controller;

import org.jetbrains.annotations.NotNull;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ru.alsu.ufanet.PoolApp.dto.ClientDTO;
import ru.alsu.ufanet.PoolApp.entity.Client;
import ru.alsu.ufanet.PoolApp.service.ClientService;
import ru.alsu.ufanet.PoolApp.util.CheckError;
import ru.alsu.ufanet.PoolApp.util.ClientErrorResponse;
import ru.alsu.ufanet.PoolApp.util.ClientNotCreateException;
import ru.alsu.ufanet.PoolApp.util.ClientNotFoundException;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v0/pool/client")
public class ClientController {

    private final ClientService clientService;
    private final ModelMapper modelMapper;
    private final CheckError checkError;

    @Autowired
    public ClientController(ClientService clientService, ModelMapper modelMapper, CheckError checkError) {
        this.clientService = clientService;
        this.modelMapper = modelMapper;
        this.checkError = checkError;
    }

    @GetMapping("/all")
    public List<ClientDTO> allClient() {
        return clientService.findAll()
                .stream()
                .map(this::convertToClientDTO)
                .collect(Collectors.toList());
    }

    @PostMapping("/get")
    @ResponseBody
    public Client getClient(@RequestBody @Valid ClientDTO clientDTO,
                            BindingResult bindingResult) {
        checkError.checkErrorClient(bindingResult);
        return clientService.findOne(clientDTO.getId());
    }

    @PostMapping("/add")
    public void saveClient(@RequestBody @Valid Client client,
                           BindingResult bindingResult) {
        this.save(client, bindingResult);
    }

    @PostMapping("/update")
    public void updateClient(@RequestBody @Valid Client client,
                             BindingResult bindingResult) {
        this.save(client, bindingResult);
    }

    private void save(Client client, BindingResult bindingResult) {
        checkError.checkErrorClient(bindingResult);
        clientService.updateClient(client);
    }

    @ExceptionHandler
    private ResponseEntity<ClientErrorResponse> handleException(ClientNotFoundException e) {
        ClientErrorResponse response = new ClientErrorResponse("The client was not found!");
        return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler
    private ResponseEntity<ClientErrorResponse> handleException(ClientNotCreateException e) {
        ClientErrorResponse response = new ClientErrorResponse(e.getMessage());
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    private ClientDTO convertToClientDTO(Client client) {
        return modelMapper.map(client, ClientDTO.class);
    }
}
