package ru.alsu.ufanet.PoolApp.controller;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;
import ru.alsu.ufanet.PoolApp.dto.RecordCancelDTO;
import ru.alsu.ufanet.PoolApp.dto.RecordDateDTO;
import ru.alsu.ufanet.PoolApp.dto.RecordIdDTO;
import ru.alsu.ufanet.PoolApp.dto.RecordSaveDTO;
import ru.alsu.ufanet.PoolApp.entity.Record;
import ru.alsu.ufanet.PoolApp.entity.RecordCount;
import ru.alsu.ufanet.PoolApp.service.ClientService;
import ru.alsu.ufanet.PoolApp.service.RecordService;
import ru.alsu.ufanet.PoolApp.util.CheckError;
import ru.alsu.ufanet.PoolApp.util.RecordErrorResponse;
import ru.alsu.ufanet.PoolApp.util.RecordSaveDTONotCreateException;


import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/v0/pool/timetable")
public class RecordController {

    private final RecordService recordService;
    private final ModelMapper modelMapper;
    private final ClientService clientService;
    private final CheckError checkError;


    @Autowired
    public RecordController(RecordService recordService, ModelMapper modelMapper, ClientService clientService, CheckError checkError) {
        this.recordService = recordService;
        this.modelMapper = modelMapper;
        this.clientService = clientService;
        this.checkError = checkError;
    }

    @GetMapping("/all")
    public List<RecordCount> getAllRecordDate(@RequestBody RecordDateDTO recordDateDTO) {
        return recordService.findAllRecordInDate(recordDateDTO.getDate());
    }

    @GetMapping("/available")
    public List<RecordCount> getAvailableRecordDate(@RequestBody RecordDateDTO recordDateDTO) {
        return recordService.findAllAvailableRecord(recordDateDTO.getDate());
    }

    @PostMapping(value = "/reserve")
    @ResponseBody
    public RecordIdDTO saveRecord(@RequestBody @Valid RecordSaveDTO recordSaveDTO,
                                  BindingResult bindingResult) {
        checkError.checkErrorRecord(bindingResult);
        return convertToRecordIdDTO(recordService.saveRecord(convectorToRecord(recordSaveDTO)));
    }

    @GetMapping(value = "/cancel")
    @ResponseBody
    public void cancelRecord(@RequestBody @Valid RecordCancelDTO recordCancelDTO,
                             BindingResult bindingResult) {
        checkError.checkErrorRecord(bindingResult);
        recordService.cancelRecord(convectorToRecord(recordCancelDTO));
    }

    private RecordIdDTO convertToRecordIdDTO(Record record) {
        return modelMapper.map(record, RecordIdDTO.class);
    }

    private Record convectorToRecord(RecordSaveDTO recordSaveDTO) {
        Record record = new Record();
        record.setClient(clientService.findOne(recordSaveDTO.getClientId()));
        record.setDateTime(recordSaveDTO.getDatetime());
        return record;
    }

    private Record convectorToRecord(RecordCancelDTO recordCancelDTO) {
        Record record = new Record();
        record.setClient(clientService.findOne(recordCancelDTO.getClientId()));
        record.setId(recordCancelDTO.getOrderId());
        return record;
    }

    @ExceptionHandler
    private ResponseEntity<RecordErrorResponse> handleException(RecordSaveDTONotCreateException e) {
        RecordErrorResponse response = new RecordErrorResponse(e.getMessage());
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }
}
