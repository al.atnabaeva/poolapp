package ru.alsu.ufanet.PoolApp.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.List;

@Entity
@Table(name = "client")
public final class Client {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "name")
    @NotEmpty(message = "No client name")
    @Size(min = 2, max = 30, message = "Strange name")
    private String name;

    @Column(name = "phone")
    @Size(min = 11, max = 12, message = "Strange phone")
    @NotEmpty(message = "No client phone")
    private String phone;

    @Column(name = "email")
    @NotEmpty(message = "No client email")
    @Email
    private String email;

    @JsonIgnore
    @OneToMany(mappedBy = "clientId")
    private List<Record> recordings;

    public Client() {
    }

    public Client(int clientId, String name) {
        this.id = clientId;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Record> getRecordings() {
        return recordings;
    }

    public void setRecordings(List<Record> recordings) {
        this.recordings = recordings;
    }
}
