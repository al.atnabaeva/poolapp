package ru.alsu.ufanet.PoolApp.entity;

import javax.validation.constraints.Size;

public class RecordCount {

    private String time;

    @Size(max = 1, message = "The record is closed")
    private int count;

    public RecordCount(String time, int count) {
        this.time = time;
        this.count = count;
    }

    public RecordCount() {
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }


}
