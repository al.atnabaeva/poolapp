package ru.alsu.ufanet.PoolApp.entity;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Entity
@Table(name = "Working_time")
public final class WorkingTime {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "name")
    @NotEmpty(message = "No client name")
    @Size(min = 2, max = 30, message = "Strange name")
    private String name;

    @Column(name = "start")
    @NotEmpty(message = "There is no time for the start of the working day")
    //@Size(min = 1, max = 2, message = "Strange name")
    private int start;

    @Column(name = "finish")
    @NotEmpty(message = "There is no time for the finish of the working day")
    //@Size(min = 1, max = 2, message = "Strange name")
    private int finish;

    public WorkingTime() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getFinish() {
        return finish;
    }

    public void setFinish(int finish) {
        this.finish = finish;
    }
}
