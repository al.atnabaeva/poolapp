package ru.alsu.ufanet.PoolApp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.alsu.ufanet.PoolApp.entity.Client;

@Repository
public interface ClientRepository extends JpaRepository<Client, Integer> {

}
