package ru.alsu.ufanet.PoolApp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.alsu.ufanet.PoolApp.entity.WorkingTime;

@Repository
public interface WorkingTimeRepository extends JpaRepository<WorkingTime, Integer> {
    @Query(value = "SELECT * FROM WorkingTime as u WHERE u.name = ?1",
            nativeQuery = true)
    WorkingTime findByName(String name);

}
