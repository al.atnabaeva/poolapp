package ru.alsu.ufanet.PoolApp.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.alsu.ufanet.PoolApp.entity.RecordCount;

import java.util.List;

@Component
public class RecordDAO {
    private final SessionFactory sessionFactory;

    @Autowired
    public RecordDAO(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Transactional(readOnly = true)
    public List<RecordCount> findAllRecord(String dateSearch) {
        Session session = sessionFactory.getCurrentSession();
        return session.createQuery("select u.time, COUNT(u.id) FROM Record as u WHERE u.date = :dateSearch Group by u.time")
                .setParameter("dateSearch", dateSearch)
                .getResultList();
    }

    @Transactional(readOnly = true)
    public List<RecordCount> findAllAvailableRecord(String dateSearch) {
        Session session = sessionFactory.getCurrentSession();
        return session.createQuery("select u.time, 10-COUNT(u.id) FROM Record as u WHERE u.date = :dateSearch Group by u.time")
                .setParameter("dateSearch", dateSearch)
                .getResultList();
    }
}
