package ru.alsu.ufanet.PoolApp.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.alsu.ufanet.PoolApp.entity.WorkingTime;

@Component
public class WorkingTimeDAO {
    private final SessionFactory sessionFactory;

    @Autowired
    public WorkingTimeDAO(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

//    public WorkingTime findStartWorkingTime(String nameDay) {
//        Session session = sessionFactory.getCurrentSession();
//        return session.createQuery("select * FROM WorkingTime as u WHERE u.name = :nameDay")
//                .setParameter("nameDay", nameDay);
//    }
}
