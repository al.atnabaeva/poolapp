package ru.alsu.ufanet.PoolApp.dto;

import javax.validation.constraints.NotNull;

public class ClientDTO {
    @NotNull(message = "There is no such client")
    private int id;

    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
