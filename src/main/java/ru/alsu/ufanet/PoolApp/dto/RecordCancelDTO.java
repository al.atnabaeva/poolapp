package ru.alsu.ufanet.PoolApp.dto;

import javax.validation.constraints.NotNull;

public class RecordCancelDTO {
    @NotNull(message = "No clientId")
    private int clientId;
    @NotNull(message = "No orderId")
    private int orderId;

    public RecordCancelDTO() {
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }
}
