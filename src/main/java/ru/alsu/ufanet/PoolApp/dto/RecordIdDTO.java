package ru.alsu.ufanet.PoolApp.dto;

public class RecordIdDTO {
    int orderId;

    public RecordIdDTO() {
    }

    public int getId() {
        return orderId;
    }

    public void setId(int orderId) {
        this.orderId = orderId;
    }
}
