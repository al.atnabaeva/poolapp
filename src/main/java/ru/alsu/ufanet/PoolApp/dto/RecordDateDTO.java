package ru.alsu.ufanet.PoolApp.dto;

import java.util.Date;

public class RecordDateDTO {

    private String date;

    private Date dateTime;

    public RecordDateDTO(String date, Date dateTime) {
        this.date = date;
        this.dateTime = dateTime;
    }

    public RecordDateDTO() {
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }
}
