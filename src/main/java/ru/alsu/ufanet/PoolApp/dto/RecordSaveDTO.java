package ru.alsu.ufanet.PoolApp.dto;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import java.util.Date;

public class RecordSaveDTO {
    private int clientId;

    @Temporal(TemporalType.DATE)
    @NotNull(message = "No date yyyy-MM-ddThh:mm:ss")
    @DateTimeFormat(pattern = "dd.MM.yyyy HH:mm:ss")
    private Date datetime;

    public RecordSaveDTO(int clientId, Date datetime) {
        this.clientId = clientId;
        this.datetime = datetime;
    }

    public RecordSaveDTO() {
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public Date getDatetime() {
        return datetime;
    }

    public void setDatetime(Date datetime) {
        this.datetime = datetime;
    }
}
