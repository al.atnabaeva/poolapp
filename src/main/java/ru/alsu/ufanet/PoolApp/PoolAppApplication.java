package ru.alsu.ufanet.PoolApp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;


@SpringBootApplication(exclude = HibernateJpaAutoConfiguration.class)
public class PoolAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(PoolAppApplication.class, args);
    }


}
