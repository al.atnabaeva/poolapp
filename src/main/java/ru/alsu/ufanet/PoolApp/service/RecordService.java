package ru.alsu.ufanet.PoolApp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.alsu.ufanet.PoolApp.dao.RecordDAO;
import ru.alsu.ufanet.PoolApp.entity.Record;
import ru.alsu.ufanet.PoolApp.entity.RecordCount;
import ru.alsu.ufanet.PoolApp.repository.RecordRepository;
import ru.alsu.ufanet.PoolApp.repository.WorkingTimeRepository;
import ru.alsu.ufanet.PoolApp.util.RecordNotSaveException;


import java.text.SimpleDateFormat;
import java.util.*;


@Service
@Transactional(readOnly = true)
public class RecordService {

    private int startWorkTime = 11;
    private int finishWorkTime = 22;

    private final RecordRepository recordRepository;
    private final RecordDAO recordDAO;

    @Autowired
    public RecordService(RecordRepository recordRepository, RecordDAO recordDAO) {
        this.recordRepository = recordRepository;
        this.recordDAO = recordDAO;

    }

    public List<RecordCount> findAllRecordInDate(String date) {
        return recordDAO.findAllRecord(date);
    }

    public Record findOne(int id) {
        Optional<Record> foundRecord = recordRepository.findById(id);
        return foundRecord.orElseThrow(RecordNotSaveException::new);
    }

    public List<RecordCount> findAllAvailableRecord(String date) {
        return recordDAO.findAllAvailableRecord(date);
    }

    @Transactional
    public void cancelRecord(Record record) {
        recordRepository.delete(record);
    }

    @Transactional
    public Record saveRecord(Record record) {
        convertDatetime(record);
        if (chek(record)) {
            return  recordRepository.save(record);
        }
        return record;
    }

    private boolean chek(Record record) {
        String time = record.getTime();
        int timeInt = Integer.parseInt(time.substring(0, 1));
        if ((timeInt >= startWorkTime) || (timeInt <= finishWorkTime)) {
            return true;
        }
        return false;
    }

    private void convertDatetime(Record record) {
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        sdf.setTimeZone(TimeZone.getTimeZone("GMT+00"));
        record.setTime(sdf.format(record.getDateTime()));
        record.setDate(new SimpleDateFormat("yyyy-MM-dd").format(record.getDateTime()));
    }
}
