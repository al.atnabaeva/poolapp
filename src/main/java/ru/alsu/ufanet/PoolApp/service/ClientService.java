package ru.alsu.ufanet.PoolApp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.alsu.ufanet.PoolApp.entity.Client;
import ru.alsu.ufanet.PoolApp.repository.ClientRepository;
import ru.alsu.ufanet.PoolApp.util.ClientNotFoundException;

import java.util.List;
import java.util.Optional;

@Service
@Transactional(readOnly = true)
public class ClientService {
    private final ClientRepository clientRepository;

    @Autowired
    public ClientService(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }

    public List<Client> findAll() {
        return clientRepository.findAll();
    }

    public Client findOne(int id) {
        Optional<Client> foundClient = clientRepository.findById(id);
        return foundClient.orElseThrow(ClientNotFoundException::new);
    }

    @Transactional
    public void updateClient(Client client) {
        clientRepository.save(client);
    }
}
