package ru.alsu.ufanet.PoolApp.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.alsu.ufanet.PoolApp.entity.WorkingTime;
import ru.alsu.ufanet.PoolApp.repository.WorkingTimeRepository;

import java.util.Calendar;
import java.util.Date;

@Service
@Transactional(readOnly = true)
public class WorkingTimeService {

    private final WorkingTimeRepository workingTimeRepository;

    public WorkingTimeService(WorkingTimeRepository workingTimeRepository) {
        this.workingTimeRepository = workingTimeRepository;
    }

    public WorkingTime findWorkingTime(WorkingTimeRepository workingTimeRepository, Date day) {
        Calendar calendar = null;
        calendar.setTime(day);
        if ((calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY)
                || ((calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY))) {
            return workingTimeRepository.findByName("workday");
        } return workingTimeRepository.findByName("weekend");
    }

    public WorkingTimeRepository getWorkingTimeRepository() {
        return workingTimeRepository;
    }
}
